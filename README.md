Use Netbeans Platform 8.0.2 (the easiest way to do this, is to simply open the project in Netbeans 8.0.2)
Do not open the project in newer Netbeans unless you know what you are doing.

The how-to-release.txt file describes how to build and release a new version.