package org.purl.jh.feat.iaa;

import java.util.Arrays;
import java.util.List;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionID;
import org.openide.util.NbBundle.Messages;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.purl.jh.feat.NbData.LLayerDataObject;
import org.purl.jh.feat.filesaction.RecFolderOutputAction;
import org.purl.jh.feat.iaa.Project2WPlainVertical.Counter;
import org.purl.net.jh.nbutil.io.IoUtils;
import org.purl.net.jh.nbutil.io.NbOutLogger;

/**
 *
 * @author jirka
 */
@ActionID(category = "Tools",
id = "org.purl.jh.feat.iaa.Project2WPlainVerticalAction")
@ActionRegistration(displayName = "#CTL_Project2WPlainVerticalAction")
@ActionReferences({
    @ActionReference(path = "Menu/Tools", position = 131),
    @ActionReference(path = "Loaders/folder/any/Actions", position = 1575),
    @ActionReference(path = "Loaders/text/feat-l+xml/Actions", position = 1575),
    @ActionReference(path = "Loaders/text/feat-a+xml/Actions", position = 1575),
    @ActionReference(path = "Loaders/text/feat-b+xml/Actions", position = 1575)
})
@Messages("CTL_Project2WPlainVerticalAction=Creates a vertical using T0 tokens + T1/2 sentences boundaries")
public class Project2WPlainVerticalAction extends RecFolderOutputAction {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(Project2WPlainVerticalAction.class);    
    
    private final Counter counter = new Project2WPlainVertical.Counter();
    
    public Project2WPlainVerticalAction(List<DataObject> context) {
        super(context, "T0 to vertical conversion", "T0 to vertical conversion");
    }

    @Override
    protected boolean beforeProcessing() {
        return warning() && super.beforeProcessing();
    }

    @Override
    protected void afterProcessing() {
        super.afterProcessing();
        // todo format
        IoUtils.println(io, "" + counter.totalWForms + "- number of all w-forms", NbOutLogger.infoColor);
    }
    
    
    
    @Override
    protected boolean isFileProcessed(FileObject aFObj) {
        return Arrays.asList("text/feat-l+xml", "text/feat-a+xml", "text/feat-b+xml").contains(aFObj.getMIMEType());
    }
    
    @Override
    public void processSingle(FileObject aFObj) {
        String fileStr = FileUtil.getFileDisplayName(aFObj);
        DataObject dobj;
        try {
            dobj = DataObject.find(aFObj);
        } catch (DataObjectNotFoundException ex) {
            userOutput.severe(ex, "The file %s cannot be found", fileStr);
            return;
        }

        try {
            ((LLayerDataObject)dobj).getData();
        } catch (Throwable ex) {
            userOutput.severe(ex, "Error loading file %s.", fileStr);
            return;
        }

        // mark paragraph and sentence boundaries by xml tags; otherwise empty line is used for both. TODO use UI
        boolean xmlStruct = true;
        
        log.info("Projecting %s.", fileStr);
        new Project2WPlainVertical((LLayerDataObject)dobj, counter, Project2WPlainVertical.Format.CONLLU, true).project();
    }
    
    // todo experimental/temporary
    
    
    
}
