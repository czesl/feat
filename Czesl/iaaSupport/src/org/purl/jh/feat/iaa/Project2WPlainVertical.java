package org.purl.jh.feat.iaa;

import cz.cuni.utkl.czesl.data.layerl.LForm;
import cz.cuni.utkl.czesl.data.layerl.LLayer;
import cz.cuni.utkl.czesl.data.layerl.Sentence;
import cz.cuni.utkl.czesl.data.layerw.WDoc;
import cz.cuni.utkl.czesl.data.layerw.WForm;
import cz.cuni.utkl.czesl.data.layerw.WLayer;
import cz.cuni.utkl.czesl.data.layerw.WPara;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openide.filesystems.FileUtil;
import org.purl.jh.feat.NbData.LLayerDataObject;
import org.purl.jh.util.err.Err;
import org.purl.jh.util.err.XException;
import org.purl.jh.util.io.IO;
import org.purl.jh.util.io.XFile;


/**
 * Creates a vertical on the basis of the W-layer, adding info about sentence boundaries from the A-layer.
 * 
 * @author jirka
 */
public class Project2WPlainVertical  {
    private final static org.purl.jh.util.Logger log = org.purl.jh.util.Logger.getLogger(Project2WPlainVertical.class);

    private final LLayerDataObject dobj;

    public static class Counter {
        int totalWForms = 0;
    }
    
    final Counter counter;
    
    enum Format {
        PLAIN, XML, CONLLU
    }
    
    Format format;
    
    boolean corr;
    
    public Project2WPlainVertical(LLayerDataObject aDobj, Counter counter, Format format, boolean corr) {
        this.dobj = aDobj;
        this.counter = counter;
        this.format = format;
        this.corr = corr;
    }

    public void project() {
        final LLayer aLayer = dobj.getData();
        final WLayer wLayer = aLayer.getWLayer();

        if (aLayer.getLowerLayer() != wLayer) return;

        project(wLayer, aLayer);
    }

    /** Get a sentence for a single wform */
    private Sentence getSentence(WForm wForm, LLayer aLayer) {
        Set<Sentence> sentences = new HashSet<>();
        for (LForm lform : wForm.getHigherForms()) {
            sentences.add(lform.getParent());
        }
        if (sentences.size() > 1) {
            Err.iAssert(false, "One form in multiple sentences! (%s)\n%s", wForm.getId().getIdStr());
        }
        return sentences.isEmpty() ? null : sentences.iterator().next();
    }
    
    /** Sentences projected on w-layer from a-layer */
    class WSentence {
        Sentence sentence;
        List<WForm> wforms;

        WSentence(Sentence sentence) {
            this.sentence = sentence;
            this.wforms =  new ArrayList<>();
        }
    }
    
    private List<WSentence> getSentences(WPara wpara, LLayer aLayer) {
        List<WSentence> sentences = new ArrayList<>();
        WSentence curSentence = null;
        List<WForm> orphans = new ArrayList<>();
        
        for (WForm wform : wpara.col()) {
            Sentence sentence = getSentence(wform, aLayer);
            if (sentence == null) {
                orphans.add(wform);
            }
            else {
                
                if (curSentence == null || sentence != curSentence.sentence) {
                    curSentence = new WSentence(sentence);
                    sentences.add(curSentence);
                }

                if (curSentence.wforms == null) throw new XException("curSentenceList == null");

                if (!orphans.isEmpty()) {
                    curSentence.wforms.addAll(orphans);
                    orphans.clear();
                }
                curSentence.wforms.add(wform);
            }
        }
        
        if (!orphans.isEmpty()) {
            if (curSentence == null) {
                log.warning("All forms in the paragraph id=%s were deleted ", wpara.getId().getIdStr());
                curSentence = new WSentence(new Sentence(aLayer, wpara.getId().getIdStr() + "-orphan"));
                sentences.add(curSentence);
            }
            
            curSentence.wforms.addAll(orphans);
        }

        return sentences;
    }
    
    
    private void project(WLayer wLayer, LLayer aLayer) {
        final XFile file = new XFile(FileUtil.toFile(dobj.getPrimaryFile())).addExtension("vert");

        try (PrintWriter w   = IO.openPrintWriter(file)) {
            for (WDoc wdoc : wLayer.col()) {
                switch (format) {
                    case PLAIN: w.printf(""); break;
                    case XML: w.printf("<doc id=\"%s\">\n", wdoc.getId().getIdStr()); break;
                    case CONLLU: w.printf("# newdoc id = %s\n", wdoc.getId().getIdStr()); break;
                }

                for (WPara wpara : wdoc.col()) {
                    switch (format) {
                        case PLAIN: w.printf("\n"); break;
                        case XML: w.printf("<para id=\"%s\">\n", wpara.getId().getIdStr()); break;
                        case CONLLU: w.printf("# newpar id = %s\n", wpara.getId().getIdStr()); break;
                    }
                            
                    for (WSentence sentence : getSentences(wpara, aLayer)) {
                        switch (format) {
                            case PLAIN: w.printf("\n"); break;
                            case XML: w.printf("<s id=\"%s\">\n", sentence.sentence.getId().getIdStr()); break;
                            case CONLLU: w.printf("# sent_id = %s\n", sentence.sentence.getId().getIdStr()); break;
                        }
                        int wFormIdx = 0;
                        for (WForm wform  : sentence.wforms) {
                            counter.totalWForms ++;
                            wFormIdx ++;
                            
                            String form = wform.getToken();
                            if (corr) {
                                LForm aform = wform.getHigherForm();
                                if (aform != null) form = aform.getToken();
                            }
                            
                            switch (format) {
                                case PLAIN: w.printf("%s\n", form); break;
                                case XML: w.printf("%s\n", form); break;
                                case CONLLU: w.printf("%d\t%s\t_\t_\t_\t_\t%d\t_\t_\t_\n", wFormIdx, form, wFormIdx-1); break;
                            }
                        }                    
                        switch (format) {
                            case PLAIN: w.printf("\n"); break;
                            case XML: w.printf("</s>\n"); break;
                            case CONLLU: w.printf("\n"); break;
                        }
                    }
                    switch (format) {
                        case PLAIN: w.printf("\n"); break;
                        case XML: w.printf("</para>\n"); break;
                        case CONLLU: w.printf("\n"); break;
                    }
                }
            }
        } catch (Throwable e) {
            log.severe(e, "writeOut");
        }
    }
}
